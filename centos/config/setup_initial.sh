#!/bin/bash
OS_ENV='redhat'
SCRIPT_PATH="/home/vagrant/config"

echo "Setting up ssh service"
source "$SCRIPT_PATH/setup_ssh.sh"
echo ""

if [[ “$OS_ENV” == *"redhat"* ]]
then
    echo "Installing ansible on Redhat OS"
    sudo yum update -y
    sudo yum install -y dnf
    sudo dnf -y install ansible
elif [[ “$OS_ENV” == *"debian"* ]]
then
    echo "Installing Ansible On Debian OS"
    sudo apt update --yes
    sudo apt install software-properties-common --yes
    sudo add-apt-repository --yes --update ppa:ansible/ansible
    sudo apt install ansible --yes
else
    echo "Operating system not defined"
fi
